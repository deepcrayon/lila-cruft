# lila-cruft

Some notes about the https://deepcrayon.fish lesser fork of lila
from https://lichess.org.

* Debian stable (bullseye/11).
* LetsEncrypt SSL.
* Non-docker.
* Apache.
* sed.

# Upstream docs
This is the main upstream docs on how to setup lila:

https://github.com/ornicar/lila/wiki/Lichess-Development-Onboarding

# Install lila
To install your own lila server, see:
```
README-install.md
```

# lila-ws
```
vim ./src/main/resources/application.conf
# set
csrf.origin = "https://deepcrayon.fish"
```

# Copying
AGPLv3+, same as upstream.

